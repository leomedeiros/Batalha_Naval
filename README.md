## EP2 - Batalha Naval 
Projeto desenvolvido por Leonardo Medeiros (170038891) para a disciplina de Orientação a Objetos, Turma 'A' - 2018.1. 

**Descrição do software:** 
Uma simples aplicação em Java produzindo uma versão Jogador vs PC do conhecido jogo Batalha Nava.

**Instruções para uso:** 
1. Download: 	
	- Baixe o código diretamente do GitLab ou pelo terminal do seu PC, usando o comando:
		`git clone https://gitlab.com/leomedeiros/Batalha_Naval`
1. Iniciando o jogo: 	
	- Para executar o software guie o terminal até o diretório  `Batalha_Naval/dist/`  e em seguida use o comando `java -jar "Batalha_Naval.jar" ` (Lembrando que é necessária a instalação previa do Java)
	- Apos executar, clique em jogar e selecione um mapa válido. (Caso não possua um mapa próprio, alguns estão disponiveis no diretório `Batalha_Naval/mapas`).
	- Por padrão seus disparos atacarão apenas uma unidade no mapa (disparo simples), use o menu à direita para alternar entre os possiveis modos de disparo.
1. Jogando:
	- Unidades marcadas com fogo indicam que você acertou um disparo naquele ponto, mas a embarcação ainda não foi afundada.
	- Unidades marcadas com fumaça indicam que você afundou uma embarcação anquele(s) ponto(s).
	- Unidades marcadas com um vortex na agua indicam que você errou um disparo naquele ponto (Tiro na agua).
	- Unidade marcadas com bolhas revelam uma embarcação naquele ponto, mas o ponto não foi atingido por disparos.
	- **Cuidado**: Disparar repetidas vezes em um mesmo ponto consumirá seus recursos. O jogo não lhe impedirá!
1. Regras:
	- Você vence ao afundar todos os navios do mapa.
	- Você perde quando seus pontos de recurso forem insuficientes para o menor dos disparos (disparo simples).


