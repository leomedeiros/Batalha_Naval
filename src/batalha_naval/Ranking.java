/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package batalha_naval;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import javax.swing.JFrame;

/**
 *
 * @author leonardo
 */
public class Ranking extends javax.swing.JFrame {

    String[] ranking;
    int[] rankingInt;
    String nomeJogador;
    int pontuacaoJogador;
    boolean fora;
    
    public Ranking(int pontuacaoJogador, String nomeJogador, boolean fora) {
        this.pontuacaoJogador = pontuacaoJogador;
        this.nomeJogador = nomeJogador;
        this.fora=fora;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setTitle("Ranking");
        initComponents();
        carregaRanking();
        insereRanking();
        salvaRanking();
        setVisible(true);

    }
    
    public void carregaRanking(){
        try{
            FileReader arq = new FileReader("outros/Ranking.txt");
            BufferedReader lerArq = new BufferedReader(arq);
            ranking = new String[5];
            rankingInt = new int[5];
            for(int i=0; i<5; i++){
                String nome;
                String pontuacao;
                nome = lerArq.readLine();
                pontuacao = lerArq.readLine();
                ranking[i] = nome+" \n"+pontuacao+"\n";
                rankingInt[i] = Integer.parseInt(pontuacao);
            }
            arq.close();
            
        }catch(IOException e){
            e.printStackTrace();
        }
    }
    
    public void insereRanking(){
        if(!fora){
            for(int i=4; i>=0; i--){
                if(pontuacaoJogador > rankingInt[i]){   
                    if(i==4) {
                        ranking[i]=nomeJogador+" \n"+String.valueOf(pontuacaoJogador)+"\n";
                    }
                    else{
                        ranking[i+1]=ranking[i];
                        ranking[i]=nomeJogador+" \n"+String.valueOf(pontuacaoJogador)+"\n";       
                    }
                }
            }
        }
        jLabel1.setText("#1  "+ranking[0]);
        jLabel2.setText("#2  "+ranking[1]);
        jLabel3.setText("#3  "+ranking[2]);
        jLabel4.setText("#4  "+ranking[3]);
        jLabel5.setText("#5  "+ranking[4]);
    }
    
    public void salvaRanking(){
        try{
            FileWriter arq = new FileWriter("outros/Ranking.txt");
            PrintWriter gravarArq = new PrintWriter(arq);
            for(int i=0; i<5; i++){
                gravarArq.printf(ranking[i]);
            }
            arq.close();
        }catch(IOException e){
            e.printStackTrace();
        }
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("jLabel1");

        jLabel2.setText("jLabel2");

        jLabel3.setText("jLabel3");

        jLabel4.setText("jLabel4");

        jLabel5.setText("jLabel5");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5))
                .addContainerGap(189, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addGap(18, 18, 18)
                .addComponent(jLabel4)
                .addGap(18, 18, 18)
                .addComponent(jLabel5)
                .addContainerGap(76, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents



    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    // End of variables declaration//GEN-END:variables
}
