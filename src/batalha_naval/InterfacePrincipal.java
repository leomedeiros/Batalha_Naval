/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package batalha_naval;

import java.awt.FlowLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * @author leonardo
 */
public class InterfacePrincipal extends javax.swing.JFrame {
    private CanvasJogo canvas = new CanvasJogo();
    private CanvasThread atualizaTelaThread = new CanvasThread(canvas);
    private int recursoJogador = 0;
    
    public InterfacePrincipal() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        initComponents();
    }
    public InterfacePrincipal(FileReader mapa){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setTitle("Batalha Naval");
        initComponents();
        try{
            canvas.constroiMapa(mapa);
        }catch(IOException e){
            JOptionPane.showMessageDialog(null, "Algo deu errado com seu mapa, reinicie o programa!");
            System.exit (0);
        }
        recursoJogador=canvas.getRecursoJogador();
        setSize(canvas.getCanvasNumeroColunas()*CanvasJogo.T_LARGURA + CanvasJogo.MARGEM*2, BotoesLaterais.ALTURA_FINAL);
        getContentPane().setLayout(new FlowLayout());
        getContentPane().add(canvas);
        BotoesLaterais menu = new BotoesLaterais(recursoJogador);
        getContentPane().add(menu);
        //pack();
        
        
        
        setVisible(true);
        atualizaTelaThread.start();
        
        canvas.addMouseListener(new MouseListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
            
            int x=e.getX();
            int y=e.getY();

            int x_pos = x/CanvasJogo.T_LARGURA;
            int y_pos = y/CanvasJogo.T_ALTURA;
            canvas.setFormaDisparo(menu.getFormaDisparo());
            canvas.setPosicao(x_pos, y_pos);
            menu.setRecursoJogador(canvas.getRecursoJogador());
            }

            @Override
            public void mouseClicked(MouseEvent e) {
            }
            @Override
            public void mousePressed(MouseEvent e) {
            }
            @Override
            public void mouseEntered(MouseEvent e) {
            }
            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(700, 500));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
