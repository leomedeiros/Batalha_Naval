/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package batalha_naval;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author leonardo
 */
public class CanvasJogo extends Canvas {
    public static final int T_LARGURA = 32;
    public static final int T_ALTURA = 32;
    public static final int MARGEM = 200;
    
    public int canvasNumeroLinhas = 15;
    public int canvasNumeroColunas = 15;
    
    private int contadorAnimacaoAgua = 1;
    private int contadorAnimacaoFogo = 1;
    private int contadorAnimacaoFumaca = 1;
    private int contadorAnimacaoAguaMarcada = 1;
    private int contadorAnimacaoBolha = 1;
    
    private int recursoJogador = 0;
    
    public static final int RECURSO_INICIAL_JOGADOR = 250;
    public static final int RECURSO_TIRO_SIMPLES = 10; //Ajustar nome para constantes?
    public static final int RECURSO_TIRO_LINHA = 60;
    public static final int RECURSO_TIRO_COLUNA = 60;
    public static final int RECURSO_TIRO_DOIS_DOIS = 35;
    public static final int RECURSO_TIRO_VER_DOIS_DOIS = 20;
    
    public int formaDisparo = 1; // 1 = tiro simples, 2 = tiro linha, 3 = tiro coluna
                             // 4 = atacar 2x2, 5 = ver 2x2;
    
    private int [][] matrizEmbarcacoes;
    private int [][] matrizSolucao;
    private int [][] matrizDisparos;
    private int [][] matrizAnimacoesDisparos;
    
    CanvasJogo(){
        
    }
    
    @Override
    public void paint(Graphics g){
        
        aumentaContadores();
        
        
        for(int i=0; i<canvasNumeroLinhas; i++){
            for(int j=0; j<canvasNumeroColunas; j++){
            
                ImageIcon iconAgua = new ImageIcon("images/agua_"+String.valueOf(contadorAnimacaoAgua)+".png");
                final Image imgAgua = iconAgua.getImage();
                ImageIcon iconFogo = new ImageIcon("images/fogo_"+String.valueOf(contadorAnimacaoFogo)+".png");
                final Image imgFogo = iconFogo.getImage();
                ImageIcon iconFumaca = new ImageIcon("images/fumaca_"+String.valueOf(contadorAnimacaoFumaca)+".png");
                final Image imgFumaca = iconFumaca.getImage();
                ImageIcon iconAguaMarcada = new ImageIcon("images/aguaMarcada_"+String.valueOf(contadorAnimacaoAguaMarcada)+".png");
                final Image imgAguaMarcada = iconAguaMarcada.getImage();
                ImageIcon iconBolha = new ImageIcon("images/bolha_"+String.valueOf(contadorAnimacaoBolha)+".png");
                final Image imgBolha = iconBolha.getImage();
                
                
                g.drawImage(imgAgua, j*T_LARGURA, i*T_ALTURA, T_LARGURA, T_ALTURA, null);
                
                if(matrizDisparos[i][j] == 1 && matrizSolucao[i][j]==1){
                    if(matrizEmbarcacoes[i][j]%10==1){
                        g.drawImage(imgFogo, j*T_LARGURA, i*T_ALTURA, T_LARGURA, T_ALTURA, null);
                    }
                    else if(matrizEmbarcacoes[i][j]%10==2){
                        g.drawImage(imgFumaca, j*T_LARGURA, i*T_ALTURA, T_LARGURA, T_ALTURA, null);
                    }
                } else if(matrizDisparos[i][j] == 1){
                    g.drawImage(imgAguaMarcada, j*T_LARGURA, i*T_ALTURA, T_LARGURA, T_ALTURA, null);
                } else if(matrizEmbarcacoes[i][j]%10==3){
                        g.drawImage(imgBolha, j*T_LARGURA, i*T_ALTURA, T_LARGURA, T_ALTURA, null);
                }
                
                if(matrizAnimacoesDisparos[i][j] > 30){
                    ImageIcon iconDisparoVer = new ImageIcon("images/disparoVer_"+String.valueOf(matrizAnimacoesDisparos[i][j]%10)+".png");
                    final Image imgDisparoVer = iconDisparoVer.getImage();
                    g.drawImage(imgDisparoVer, j*T_LARGURA, i*T_ALTURA, T_LARGURA, T_ALTURA, null);
                    if(matrizAnimacoesDisparos[i][j]%10==9){
                        matrizAnimacoesDisparos[i][j]=0;
                    }else{
                        matrizAnimacoesDisparos[i][j]++;
                    }
                }else if(matrizAnimacoesDisparos[i][j] > 20){
                    ImageIcon iconDisparoAgua = new ImageIcon("images/disparoAgua_"+String.valueOf(matrizAnimacoesDisparos[i][j]%10)+".png");
                    final Image imgDisparoAgua = iconDisparoAgua.getImage();
                    g.drawImage(imgDisparoAgua, j*T_LARGURA, i*T_ALTURA, T_LARGURA, T_ALTURA, null);
                    if(matrizAnimacoesDisparos[i][j]%10==3){
                        matrizAnimacoesDisparos[i][j]=0;
                    }else{
                        matrizAnimacoesDisparos[i][j]++;
                    }
                }else if(matrizAnimacoesDisparos[i][j] > 10){
                    ImageIcon iconDisparoEmbarcacao = new ImageIcon("images/disparoEmbarcacao_"+String.valueOf(matrizAnimacoesDisparos[i][j]%10)+".png");
                    final Image imgDisparoEmbarcacao = iconDisparoEmbarcacao.getImage();
                    g.drawImage(imgDisparoEmbarcacao, j*T_LARGURA, i*T_ALTURA, T_LARGURA, T_ALTURA, null);
                    if(matrizAnimacoesDisparos[i][j]%10==4){
                        matrizAnimacoesDisparos[i][j]=0;
                    }else{
                        matrizAnimacoesDisparos[i][j]++;
                    }
                }

            }
           
        }
    }
    
    public void constroiMapa(FileReader mapa) throws IOException{
        //Ler o mapa
        recursoJogador=CanvasJogo.RECURSO_INICIAL_JOGADOR;
        BufferedReader lerMapa = new BufferedReader(mapa);
        String linha = lerMapa.readLine(); // A primeira linha nao interessa
        linha = lerMapa.readLine(); // A segunda contém largura e altura
        int temp1, temp2;
        char [] array = linha.toCharArray();
        if(array[1]==' '){
            temp1 = (int)(array[0]-'0');
            if(linha.length()==3){
                temp2 = (int)(array[2]-'0');
            }else{
                temp2 = (int)((array[2]-'0')*10 + array[3]-'0');
            }
        }else{
            temp1 = (int)((array[0]-'0')*10 + array[1]-'0');
            if(linha.length()==5){
                temp2 = (int)((array[3]-'0')*10 + array[4]-'0');
            }else{
                temp2 = (int)(array[3]-'0');
            }
            
        }
        canvasNumeroColunas = temp1;
        canvasNumeroLinhas = temp2;
        matrizEmbarcacoes = new int [canvasNumeroLinhas][canvasNumeroColunas];
        matrizSolucao = new int [canvasNumeroLinhas][canvasNumeroColunas];
        matrizDisparos = new int [canvasNumeroLinhas][canvasNumeroColunas];
        matrizAnimacoesDisparos = new int [canvasNumeroLinhas][canvasNumeroColunas];
        setSize(canvasNumeroColunas*T_LARGURA, canvasNumeroLinhas*T_ALTURA);
        //TESTANDO A LEITURA
        //JOptionPane.showMessageDialog(null, ("(DEBUG)Mapa : " + String.valueOf(temp2)+"x"+String.valueOf(temp1)));
        
        linha = lerMapa.readLine();
        linha = lerMapa.readLine();
        for(int i=0; i<canvasNumeroLinhas; i++){
            linha = lerMapa.readLine();
            char  linhaArray[] = linha.toCharArray();
            for(int j=0; j<canvasNumeroColunas; j++){
                matrizEmbarcacoes[i][j]=10*(int)(linhaArray[j]-'0');
                if(matrizEmbarcacoes[i][j]!=0){
                    matrizSolucao[i][j]=1;
                }
            }
        }
        while(linha != null){ // O Resto nao interessa
            linha = lerMapa.readLine();
        }
        
        
    }
    
    public void setPosicao(int x_pos,int y_pos){
        //JOptionPane.showMessageDialog(null, (String.valueOf(y_pos) + ", " + String.valueOf(x_pos)));
        switch(formaDisparo){
            case 1:
                if(recursoJogador - RECURSO_TIRO_SIMPLES >= 0){
                    recursoJogador -= RECURSO_TIRO_SIMPLES;
                    disparo(y_pos, x_pos);
                }else{
                    JOptionPane.showMessageDialog(null, "Você não possui recursos suficientes para efetuar este disparo!");
                }
                break;
            case 2:
                if(recursoJogador - RECURSO_TIRO_LINHA >= 0){
                    recursoJogador -= RECURSO_TIRO_LINHA;
                    for(int i=0; i<canvasNumeroColunas; i++){
                        disparo(y_pos, i);
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Você não possui recursos suficientes para efetuar este disparo!");
                }
                break;
            case 3:
                if(recursoJogador - RECURSO_TIRO_COLUNA >= 0){
                    recursoJogador -= RECURSO_TIRO_COLUNA;
                    for(int i=0; i<canvasNumeroLinhas; i++){
                        disparo(i, x_pos);
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Você não possui recursos suficientes para efetuar este disparo!");
                }
                break;
            case 4:
                if(recursoJogador - RECURSO_TIRO_DOIS_DOIS >= 0){
                    recursoJogador -= RECURSO_TIRO_DOIS_DOIS;
                    disparo(y_pos, x_pos);
                    disparo(y_pos+1, x_pos);
                    disparo(y_pos, x_pos+1);
                    disparo(y_pos+1, x_pos+1);
                }else{
                    JOptionPane.showMessageDialog(null, "Você não possui recursos suficientes para efetuar este disparo!");
                }
                break;
            case 5:
                if(recursoJogador - RECURSO_TIRO_VER_DOIS_DOIS >= 0){
                    recursoJogador -= RECURSO_TIRO_VER_DOIS_DOIS;  
                    visualiza(y_pos, x_pos);
                    visualiza(y_pos+1, x_pos);
                    visualiza(y_pos, x_pos+1);
                    visualiza(y_pos+1, x_pos+1);
                }else{
                    JOptionPane.showMessageDialog(null, "Você não possui recursos suficientes para efetuar este disparo!");
                }
                break;
            default:
                JOptionPane.showMessageDialog(null, ("Algo muito errado aconteceu!\nPor favor reporte este bug!"));
                System.exit (0);
                break;
        }
        verificaVitoria();
        verificaDerrota();
    }
    
    public void disparo(int i, int j){
        if(matrizDisparos[i][j]==0){
            matrizDisparos[i][j]=1;
            if(matrizEmbarcacoes[i][j]!=0){
                matrizEmbarcacoes[i][j]=(matrizEmbarcacoes[i][j]/10)*10+1;
            }
            if(matrizEmbarcacoes[i][j]/10!=0){
                matrizAnimacoesDisparos[i][j]=11;
            }else{
                matrizAnimacoesDisparos[i][j]=21;
            }
            verificaNaviosAfundados(i, j);
        }
    }
    
    public void visualiza(int i, int j){
        if(matrizEmbarcacoes[i][j]!=0 && matrizDisparos[i][j]==0){
            matrizEmbarcacoes[i][j]=(matrizEmbarcacoes[i][j]/10)*10+3;
        }
        matrizAnimacoesDisparos[i][j]=31;
    }
    
    public void verificaVitoria(){
        boolean vitoria=true;
        for(int i=0; i<canvasNumeroLinhas; i++){
            for(int j=0; j<canvasNumeroColunas; j++){
                if(matrizSolucao[i][j]==1 && matrizDisparos[i][j]!=1){
                    vitoria=false;
                    break;
                }
            }
        }
        if(vitoria){
            int pontuacaoJogador = recursoJogador*10; 
            JOptionPane.showMessageDialog(null, ("Você venceu!\nSua pontuação foi de: "+String.valueOf(pontuacaoJogador)+"."));
            ControladorRanking controle = new ControladorRanking(pontuacaoJogador);
            //System.exit (0);
        }
    }
    
    public void verificaDerrota(){
        if(recursoJogador < RECURSO_TIRO_SIMPLES){
            JOptionPane.showMessageDialog(null, ("Seus recursos são insuficientes.\nVocê foi derrotado!\nBoa sorte na próxima, marinheiro!"));
            System.exit (0);
        }
        
    }
    public void verificaNaviosAfundados(int i, int j){
        for(int k=0; k<canvasNumeroLinhas; k++){
            if(matrizEmbarcacoes[k][j]/10!=0){ // Se há um navio && ele não foi visitado nessa querry
                boolean afundado=true;
                if(matrizEmbarcacoes[k][j]/10+k<=canvasNumeroLinhas){
                    for(int l=k; l<matrizEmbarcacoes[k][j]/10+k; l++){
                        if(matrizEmbarcacoes[l][j]%10!=1){
                            afundado=false;
                        }
                        if(matrizEmbarcacoes[l][j]/10 != matrizEmbarcacoes[k][j]/10){
                            afundado=false;
                        }
                        if(l!=k){
                            if(matrizEmbarcacoes[l][j]%10!=matrizEmbarcacoes[l-1][j]%10){
                                afundado=false;
                            }
                        }
                    }
                    if(afundado){
                        for(int l=k; l<matrizEmbarcacoes[k][j]/10+k; l++){
                            matrizEmbarcacoes[l][j]=(matrizEmbarcacoes[l][j]/10)*10+2;
                        }
                    }
                }
            }
        }
        
        for(int k=0; k<canvasNumeroColunas; k++){
            if(matrizEmbarcacoes[i][k]/10!=0){
                boolean afundado=true;
                if(matrizEmbarcacoes[i][k]/10+k<=canvasNumeroColunas){
                    for(int l=k; l<matrizEmbarcacoes[i][k]/10+k; l++){
                        if(matrizEmbarcacoes[i][l]%10!=1){
                            afundado=false;
                        }
                        if(matrizEmbarcacoes[i][l]/10 != matrizEmbarcacoes[i][k]/10){
                            afundado=false;
                        }
                    }
                    if(afundado){
                        for(int l=k; l<matrizEmbarcacoes[i][k]/10+k; l++){
                            matrizEmbarcacoes[i][l]=(matrizEmbarcacoes[i][l]/10)*10+2;
                        }
                    }
                }
            }
        }
    }
    
    public int getCanvasNumeroLinhas(){
        return canvasNumeroLinhas;
    }
    
    public int getCanvasNumeroColunas(){
        return canvasNumeroColunas;
    }
    
    public void aumentaContadores(){
        if(contadorAnimacaoAgua >= 14){
            contadorAnimacaoAgua=1;
        }else{
            contadorAnimacaoAgua++;
        }
        
        if(contadorAnimacaoFogo >= 5){
            contadorAnimacaoFogo=1;
        }else{
            contadorAnimacaoFogo++;
        }
        
        if(contadorAnimacaoFumaca >= 7){
            contadorAnimacaoFumaca=1;
        }else{
            contadorAnimacaoFumaca++;
        }
        
        if(contadorAnimacaoAguaMarcada >= 15){
            contadorAnimacaoAguaMarcada=1;
        }else{
            contadorAnimacaoAguaMarcada++;
        }
        
        if(contadorAnimacaoBolha >= 8){
            contadorAnimacaoBolha=1;
        }else{
            contadorAnimacaoBolha++;
        }
    }
    
    public void setFormaDisparo(int formaDisparo){
        this.formaDisparo = formaDisparo;
    }
    
    public int getFormaDisparo(){
        return formaDisparo;
    }

    public int getRecursoJogador(){
        return recursoJogador;
    }
    
}
