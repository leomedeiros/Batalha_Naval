/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package batalha_naval;

import javax.swing.JOptionPane;

/**
 *
 * @author leonardo
 */
public class CanvasThread extends Thread {
    
    private CanvasJogo canvas;
    public boolean executando=true;
    
    CanvasThread (CanvasJogo canvas){
        this.canvas = canvas;
        
    }
    
    @Override
    public void run(){
        while(executando){
            try{
                Thread.sleep(100);
            } catch(InterruptedException e){
                e.printStackTrace();
            }
            canvas.paint(canvas.getGraphics());
        }
        
    }
}
